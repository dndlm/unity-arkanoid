﻿using UnityEngine;
using System.Collections;

public class PaddleMovement : MonoBehaviour {

    [SerializeField]
    private bool mouseControl;
    [SerializeField]
    private float v;

    private SpriteRenderer sRenderer;
    private float anchoPaleta; 

    // Use this for initialization
    void Start () {
        sRenderer = GetComponent<SpriteRenderer>();
        anchoPaleta = sRenderer.bounds.size.x;
    }
	
	// Update is called once per frame
	void Update () {
       
        if (mouseControl) { 
            transform.position = new Vector3(
                Mathf.Clamp(16 * Input.mousePosition.x / Screen.width - 8F, -8F + anchoPaleta/2, 8F - anchoPaleta/2),
                transform.position.y, 
                transform.position.z
            );
        } else {
            if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow)) {
                transform.position = new Vector3(
                    Mathf.Clamp( transform.position.x - v * Time.deltaTime, -8F + anchoPaleta / 2, 8F - anchoPaleta / 2),
                    transform.position.y,
                    transform.position.z
                    );
            } else  if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow)) {
                transform.position = new Vector3(
                    Mathf.Clamp(transform.position.x + v * Time.deltaTime, -8F + anchoPaleta / 2, 8F - anchoPaleta / 2),
                    transform.position.y,
                    transform.position.z
                    );
            }
        }
    }
}
