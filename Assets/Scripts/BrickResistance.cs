﻿using UnityEngine;
using System.Collections;

public class BrickResistance : MonoBehaviour {

    [SerializeField]
    private int _maxHits;
    [SerializeField]
    private Sprite[] hitSprites;

    private int _hits;
    private SpriteRenderer spRenderer;

    void Start() {
        spRenderer = GetComponent<SpriteRenderer>();
    }

    void OnCollisionEnter2D(Collision2D col) {

        _hits++;
        if(_hits >= _maxHits) {
            Destroy(gameObject);
        } else {
            spRenderer.sprite = hitSprites[_hits - 1];
        }
       
    }
}
